import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IGenericTableModel } from './models/title.model';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
  @Input()titles: Array<IGenericTableModel>;
  @Input()tableName: string;
  @Input()items: Array<object>;
  @Input()selectName: string;
  @Input()selectItems: Array<{key: string, value: string}>;

  @Output()findMore = new EventEmitter();
  @Output()selected = new EventEmitter<string>();
  @Output()indexItem = new EventEmitter<number>();
  @Output()definedFilters = new EventEmitter<Array<{key: string, value: string}>>();

  isFullListDisplayed: boolean;
  noOfItemsToShowInitially: number;
  lastItemsLenght: number;
  filters: Array<IGenericTableModel>;

  constructor() {}

  ngOnInit() {
    this.filters = [];
    this.isFullListDisplayed = false;
    this.titles.forEach(teste => {
      this.filters.push({key: teste.key, value: undefined});
    });
  }

  onScroll() {
    if (!this.lastItemsLenght || (this.lastItemsLenght < this.items.length)) {
      this.lastItemsLenght = this.items.length;
      this.findMore.emit();
    } else {
      this.isFullListDisplayed = true;
    }
  }

  onFilter(key: string, value: string) {
    this.filters.forEach(teste => {
      if (teste.key === key) {
        teste.value = value;
      }
    });

    this.definedFilters.emit(this.filters);
  }
}
