export class Contract {
  constructor(
    public name: string,
    public description: string,
    public assignment: string
  ) {}
}
