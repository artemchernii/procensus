import { contracts } from './contracts';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ContractsService {
  constructor() {}

  getContracts() {
    return contracts;
  }
}
