import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.scss']
})
export class ClientComponent implements OnInit {
  public showPreview: boolean = false;
  public cardOrList: boolean = false;
  constructor() {}

  ngOnInit() {}

  onOptionSelected(itemValue: any) {
    if (itemValue.source.value === 'value-1') {
      this.showPreview = true;
    } else {
      this.showPreview = false;
    }
  }
}
