export interface IGenericTableModel {
  key: string;
  value: string;
}
