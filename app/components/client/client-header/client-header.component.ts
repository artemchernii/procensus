import { ContractsService } from './../../interventions/contracts.service';
import {
  Component,
  OnInit,
  ViewEncapsulation,
  Output,
  EventEmitter
} from '@angular/core';
export interface Items {
  value: string;
  viewValue: string;
}
@Component({
  selector: 'app-client-header',
  templateUrl: './client-header.component.html',
  styleUrls: ['./client-header.component.scss'],
  encapsulation: ViewEncapsulation.Native
})
export class ClientHeaderComponent implements OnInit {
  @Output() selectOption = new EventEmitter();
  items: Items[] = [
    { value: 'value-1', viewValue: 'Em planeamento 1' },
    { value: 'value-2', viewValue: 'Em planeamento 2' },
    { value: 'value-3', viewValue: 'Em planeamento 3' }
  ];
  contracts = [];
  constructor(private contractsService: ContractsService) {}

  ngOnInit() {
    this.contracts = this.contractsService.getContracts();
  }
  hideBar(itemValue: any) {
    this.selectOption.emit(itemValue);
  }
}
