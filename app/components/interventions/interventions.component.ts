import { Contract } from './contract.model';
import { ContractsService } from './contracts.service';
import { Component, OnInit } from '@angular/core';

export interface Items {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-interventions',
  templateUrl: './interventions.component.html',
  styleUrls: ['./interventions.component.scss']
})
export class InterventionsComponent implements OnInit {
  public contracts = [];
  public newcontract: string = '';
  private noOfItemsToShowInitially: number = 20;
  private itemsToLoad: number = 10;

  private itemsList = this.contractsService.getContracts();
  public itemsToShow = this.itemsList.slice(0, this.noOfItemsToShowInitially);
  public isFullListDisplayed: boolean = false;
  items: Items[] = [
    { value: 'Em planeamento-0', viewValue: 'Em planeamento 21' },
    { value: 'Em planeamento-1', viewValue: 'Em planeamento 32' },
    { value: 'Em planeamento-2', viewValue: 'Em planeamento 21' }
  ];
  constructor(private contractsService: ContractsService) {}

  ngOnInit() {
    this.getContracts();
  }

  getContracts() {
    this.contracts = this.contractsService.getContracts();
  }

  onScroll() {
    if (this.noOfItemsToShowInitially <= this.itemsList.length) {
      this.noOfItemsToShowInitially += this.itemsToLoad;
      this.itemsToShow = this.itemsList.slice(0, this.noOfItemsToShowInitially);
      console.log('scrolled');
    } else {
      this.isFullListDisplayed = true;
    }
  }
}
