import { Component, OnInit } from '@angular/core';

export interface Obra {
  id: number;
  LOC: string;
  ITMREF: number;
  ITMDES: string;
  PCU: number;
  QTYDISP: number;
  QTYVAL: number;
  QTYNVAL: number;
}

const ELEMENT_DATA: Obra[] = [
  {
    id: 1,
    LOC: 'Odivelas',
    ITMREF: 123,
    ITMDES: 'Huge',
    PCU: 555,
    QTYDISP: 91239,
    QTYVAL: 2,
    QTYNVAL: 5
  },
  {
    id: 2,
    LOC: 'Odivelas',
    ITMREF: 123,
    ITMDES: 'Huge',
    PCU: 555,
    QTYDISP: 91239,
    QTYVAL: 2,
    QTYNVAL: 5
  },
  {
    id: 3,
    LOC: 'Odivelas',
    ITMREF: 123,
    ITMDES: 'Huge',
    PCU: 555,
    QTYDISP: 91239,
    QTYVAL: 2,
    QTYNVAL: 5
  },
  {
    id: 4,
    LOC: 'Odivelas',
    ITMREF: 123,
    ITMDES: 'Huge',
    PCU: 555,
    QTYDISP: 91239,
    QTYVAL: 2,
    QTYNVAL: 5
  },
  {
    id: 5,
    LOC: 'Odivelas',
    ITMREF: 123,
    ITMDES: 'Huge',
    PCU: 555,
    QTYDISP: 91239,
    QTYVAL: 2,
    QTYNVAL: 5
  }
];

@Component({
  selector: 'app-stock-obra',
  templateUrl: './stock-obra.component.html',
  styleUrls: ['./stock-obra.component.scss']
})
export class StockObraComponent implements OnInit {
  displayedColumns: string[] = [
    'id',
    'LOC',
    'ITMREF',
    'ITMDES',
    'PCU',
    'QTYDISP',
    'QTYVAL',
    'QTYNVAL'
  ];
  dataSource = ELEMENT_DATA;
  constructor() {}

  ngOnInit() {}
}
