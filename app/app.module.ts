import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { SharedModule } from './shared/shared.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './shared/template/navbar/navbar.component';
import { CustomMaterialModule } from './shared/custom-material/material.module';
import {
  BrowserAnimationsModule,
  NoopAnimationsModule
} from '@angular/platform-browser/animations';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { Routes, RouterModule } from '@angular/router';

import { HeaderComponent } from './shared/template/header/header.component';
import { AuthComponent } from './components/auth/auth.component';
import { InterventionsComponent } from './components/interventions/interventions.component';
import { FormsModule } from '@angular/forms';
import { ClientComponent } from './components/client/client.component';
import { ClientHeaderComponent } from './components/client/client-header/client-header.component';
import { ClientDetailsComponent } from './components/client/client-details/client-details.component';
import { ConstructionListComponent } from './components/client/construction-list/construction-list.component';
import { StockObraComponent } from './components/client/stock-obra/stock-obra.component';
import { StockDetailsComponent } from './components/client/stock-details/stock-details.component';

// ADICIONAR EM OUTRO MODULE
import { TableComponent } from './shared/table/table.component';

const appRoutes: Routes = [
  { path: '', component: AuthComponent },
  { path: 'interventions', component: InterventionsComponent },
  { path: 'client', component: ClientComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    NavbarComponent,
    AuthComponent,
    InterventionsComponent,
    ClientComponent,
    ClientHeaderComponent,
    ClientDetailsComponent,
<<<<<<< HEAD
    ConstructionListComponent,
    StockObraComponent,
    StockDetailsComponent
=======
    // ADICIONAR EM OUTRO MODULE
    TableComponent
>>>>>>> e305845795f610ac1c3c582c82b9ebfc2cc6a3d3
  ],
  imports: [
    BrowserModule,
    SharedModule,
    AppRoutingModule,
    FormsModule,
    CustomMaterialModule,
    BrowserAnimationsModule,
    NoopAnimationsModule,
    RouterModule.forRoot(appRoutes),
    InfiniteScrollModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
